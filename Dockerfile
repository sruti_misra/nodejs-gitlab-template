FROM node:8-alpine
MAINTAINER Sruti Misra (sruti.misra@infostretch.com)
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . /usr/src/app
RUN npm install
EXPOSE 5000
CMD [ "npm", "start" ]
